import React, { useState } from "react";
import { Grid, Button } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import data from "../../import.json";
import Text from "../FormFields/TextField";
import Select from "../FormFields/Select";
import Radio from "../FormFields/Radio";
import Number from "../FormFields/Number";
import Switch from "../FormFields/Switch";
import Slider from "../FormFields/Slider";
import DateField from "../FormFields/DateField";
import Checkbox from "../FormFields/Checkbox";

const useStyles = makeStyles((theme) => ({
  buttons: {
    display: "flex",
    justifyContent: "flex-end",
  },
  button: {
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(1),
  },
}));

export default function FormContent() {
  const classes = useStyles();
  const [state, setState] = useState({});
  const fields = data.data;

  console.log(state)
  function handleSubmit(e) {
    e.preventDefault();
    alert(JSON.stringify(state));
  }

  const handleSwitch = (e) => {
    console.log(e)
    setState({ ...state, [e.target.name]: e.target.checked });
  };

  const handleChange = (e) => {
    const {
      target: { value, name },
    } = e;

    setState((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  const handleDate = (name, date) => {
    date = date.toISOString().split("T")[0]
    setState({...state, [name]: date})
  }

  const handleSlide = (name, value) => {
    setState({...state, [name]: [value]})
  }

  return (
    <form onSubmit={handleSubmit}>
      <Grid container spacing={4}>
        {fields.map((field, index) => {
          return field.Type === "text" ? (
            <Grid item xs={12} key={index}>
              <Text field={field} handleChange={handleChange} state={state} />
            </Grid>
          ) : field.Type === "select" ? (
            <Grid item xs={6} key={index}>
              <Select field={field} handleChange={handleChange} state={state} />
            </Grid>
          ) : field.Type === "numeric" ? (
            <Grid item xs={6} key={index}>
              <Number field={field} handleChange={handleChange} state={state} />
            </Grid>
          ) : field.Type === "radio" ? (
            <Grid item xs={6} key={index}>
              <Radio field={field} handleChange={handleChange} state={state} />
            </Grid>
          ) : field.Type === "check" ? (
            <Grid item xs={6}>
              <Checkbox field={field} handleSlide={handleSlide} state={state} />
            </Grid>
          ) : field.Type === "switch" ? (
            <Grid item xs={12}>
              <Switch field={field} handleSwitch={handleSwitch} state={state} />
            </Grid>
          ) : field.Type === "slide" ? (
            <Grid item xs={12}>
              <Slider field={field} handleSlide={handleSlide} state={state} />
            </Grid>
          ) : field.Type === "date" ? (
            <Grid item xs={6}>
              <DateField
                field={field}
                handleDate={handleDate}
                state={state}
              />
            </Grid>
          ) : null;
        })}
        <Grid item xs={12} className={classes.buttons}>
          <Button
            variant="contained"
            color="primary"
            type="submit"
            className={classes.buttons}
          >
            Submit
          </Button>
        </Grid>
      </Grid>
    </form>
  );
}
