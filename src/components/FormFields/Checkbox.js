import React, { useState } from "react";
import {
  FormControl,
  FormControlLabel,
  FormLabel,
  FormGroup,
  Checkbox,
} from "@material-ui/core";

const Check = (props) => {
  const handleChange = (event) => {
    props.handleSlide(event.target.name, event.target.checked);
  };

  return (
    <FormControl>
      <FormLabel>{props.field.Name}</FormLabel>
      <FormGroup
        value={props.state[props.field.Name]}
        required={props.field.Required}
        name={props.field.Name}
        onChange={handleChange}
      >
        {props.field.Values.split(",").map((item, index) => {
          return (
            <FormControlLabel
              value={item}
              key={index}
              control={
                <Checkbox
                  checked={props.state[props.field.Name]}
                  name={item}
                  color="primary"
                />
              }
              label={item}
            />
          );
        })}
      </FormGroup>
    </FormControl>
  );
};
export default Check;
