import React from "react";
import { TextField, FormControl } from "@material-ui/core";

const Number = (props) => {
  return (
    <FormControl fullWidth>
      <TextField
        required={props.field.Required}
        inputProps={{ type: "number" }}
        label={props.field.Name}
        name={props.field.Name}
        onChange={props.handleChange}
      >
        {props.field.Name}
      </TextField>
    </FormControl>
  );
};

export default Number;
