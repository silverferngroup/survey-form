import React from "react";
import {
  FormControl,
  FormControlLabel,
  FormLabel,
  RadioGroup,
  Radio,
} from "@material-ui/core";

const RadioField = (props) => {
  return (
    <FormControl>
      <FormLabel>{props.field.Name}</FormLabel>
      <RadioGroup
        value={props.state[props.field.Name]}
        required={props.field.Required}
        name={props.field.Name}
        onChange={props.handleChange}
      >
        {props.field.Values.split(",").map((item, index) => {
          return (
            <FormControlLabel
              value={item}
              key={index}
              control={<Radio color="primary"/>}
              label={item}
            />
          );
        })}
      </RadioGroup>
    </FormControl>
  );
};
export default RadioField;
