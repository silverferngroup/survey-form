import React from "react";
import { InputLabel, Select, MenuItem, FormControl } from "@material-ui/core";

const SelectField = (props) => {
  return (
    <FormControl fullWidth>
      <InputLabel required={props.field.Required}>
        {props.field.Name}
      </InputLabel>
      <Select
        id={props.field.Name}
        name={props.field.Name}
        value={props.state[props.field.Name]}
        width="100%"
        onChange={props.handleChange}
        defaultValue={props.field.Default}
      >
        {props.field.Values.split(",").map((item, index) => {
          return (
            <MenuItem key={index} value={item}>
              {item}
            </MenuItem>
          );
        })}
      </Select>
    </FormControl>
  );
};

export default SelectField;
