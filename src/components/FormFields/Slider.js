import React, { useState } from "react";
import { Typography, Slider, FormControl } from "@material-ui/core";

const Slide = (props) => {

  const [value, setValue] = React.useState([20, 37]);

  const handleChange = (e, newValue) => {
    setValue(newValue)
    props.handleSlide(props.field.Name, value);
  };

  function valuetext(value) {
    return value;
  }

  return (
    <FormControl fullWidth>
      <Typography id="range-slider" gutterBottom>
        {props.field.Name}
      </Typography>
      <Slider
        name={props.field.Name}
        value={value}
        onChange={handleChange}
        valueLabelDisplay="auto"
        aria-labelledby="range-slider"
        getAriaValueText={valuetext}
      />
    </FormControl>
  );
};

export default Slide;
