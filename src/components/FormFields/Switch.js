import React from "react";
import { Grid, FormControl, FormControlLabel, Switch } from "@material-ui/core";

const SwitchField = (props) => {
  return (
    <FormControl>
      <FormControlLabel
        control={
          <Switch
            color="primary"
            checked={props.state[props.field.Name]}
            onChange={props.handleSwitch}
            name={props.field.Name}
          />
        }
        label={props.field.Name}
      />
    </FormControl>
  );
};

export default SwitchField;
