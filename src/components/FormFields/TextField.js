import React from "react";
import {
    TextField,
    FormControl,
  } from "@material-ui/core";

const Text = (props) => {
  return (
    <FormControl fullWidth>
      <TextField
        required={props.field.Required}
        id={props.field.Name}
        name={props.field.Name}
        label={props.field.Name}
        fullWidth
        onChange={props.handleChange}
      />
    </FormControl>
  );
};

export default Text;
